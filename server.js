const http=require("http");
const fs = require("fs");
const uuid = require ("uuid4");


//HANDLING "UUID"
let uuidJSON = { "uuid" : uuid() };
let jsonString = JSON.stringify(uuidJSON);

fs.writeFile("./public/uuid.json", jsonString, "utf8", err => {
    if (err) {
      console.error(err);
    }
  });


const readFilePromise=(filename)=>{

    return new Promise((resolve,reject)=>{
        fs.readFile(filename, "UTF-8" , (err, fileContent)=>{
            if(err){
                reject(err);
            }else{
                resolve(fileContent);
            }
        });
    });

}


const errorHandlerCallback=(request, response, err )=>{
    console.error(err);
    response.writeHead(500,{
      'Content-Type' : 'application/json',
  });
  response.write(JSON.stringify({
    message : "internal server error",
  }))
  response.end();
  }


const server= http.createServer((request,response)=>{

        //HANDLING THE STATUS CODE CASE
        let status_code = "";
        let status_string = "";
        let url_string = request.url;
        if(url_string.slice(0,7) == "/status"){
            status_string = url_string;
            status_code = url_string.slice(8);
        }

        // HANDLING DELAY CASE
        let delay_time = "";
        let delay_string = "";
        if(url_string.slice(0,6) == "/delay"){
            delay_string = url_string;
            delay_time = url_string.slice(7);
        }


    switch(request.url){

        case "/html" : {
            readFilePromise("./public/index.html")
            .then(data=>{
                response.writeHead(200,{"Content-Type" : "text/html"})
                response.write(data);
                response.end();
            })
            .catch(err=>{
                errorHandlerCallback(response,err);
            })
            break;
        }

        case "/json" : {
            readFilePromise("./public/index.json")
            .then(data=>{
                response.writeHead(200, {"Content-Type" : "application/json"})
                response.write(data);
                response.end();
            })
            .catch(err=>{
                errorHandlerCallback(response,err);
            });
            break;
        }

        case "/uuid" : {
            readFilePromise("./public/uuid.json")
            .then(data=>{
                response.writeHead(200, {"Content-Type" : "application/json"})
                response.write(data);
                response.end();
            })
            .catch(err=>{
                errorHandlerCallback(response,err);
            });
            break;
        }

        case `${status_string}` : {

            response.writeHead(Number(status_code),{"Content-Type" : "text/plain"})
            response.write(`RESPONSE FOR STATUS CODE ${status_code}`);
            response.end();
            break;
        }
        
        case `${delay_string}` : {
            let time_in_number = Number(delay_time)*1000;
            setTimeout(()=>{
                response.writeHead(200,{"Content-Type" : "text/plain"})
                response.write(`RESPONSE IS SHOWING AFTER ${time_in_number} ms`);
                response.end();
            },time_in_number);

            break;
        }
        
        default:{
            response.writeHead(404,{"Content-Type":"text/html"})
            response.write("<strong>404 Not Found</strong>");
            response.end();
      
        }
    }
});

server.listen(8000);
console.log(`server is listening on port : 8000.......`);